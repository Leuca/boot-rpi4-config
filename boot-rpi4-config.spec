%define debug_package %{nil}

Name:           boot-rpi4-config
Version:        0
Release:        5%{?dist}
Summary:        Package that configures direct kernel boot on a Raspberry Pi 4
License:        MIT

Source0:        config.txt
    
BuildRequires:  efi-filesystem
BuildRequires:  efi-srpm-macros

Requires:       bcm2711-firmware

Requires:       kernel

Conflicts:      edk2-rpi4
Conflicts:      uboot-rpi4-config

%description
Simple package that installs the right files in the right places to allow a Raspberry Pi to boot with kernel direct boot

%prep

%build

%install
mkdir -p %{buildroot}%{efi_esp_root}
install -m 0600 %{SOURCE0} %{buildroot}%{efi_esp_root}

%post
# Create cmdline.txt with grubby
if [ ! -f %{efi_esp_root}/cmdline.txt ]
then
    echo "root="$(grubby --info=$(grubby --default-kernel) | grep root= | cut -d"\"" -f 2) \
    $(grubby --info=$(grubby --default-kernel) | grep args | cut -d"\"" -f 2) \
        > %{efi_esp_root}/cmdline.txt
fi

# Copy the kernel and initramfs
ver=$(grubby --info=$(grubby --default-kernel) | grep kernel=\" | cut -d"\"" -f 2 | cut -d"-" -f 2)
rel=$(grubby --info=$(grubby --default-kernel) | grep kernel=\" | cut -d"\"" -f 2 | cut -d"-" -f 3)
vmlinuz=/boot/vmlinuz-$ver-$rel
initramfs=/boot/initramfs-$ver-$rel.img
install $vmlinuz %{efi_esp_root}/vmlinuz
install $initramfs %{efi_esp_root}/initramfs

%files
%config(noreplace) %{efi_esp_root}/config.txt

%changelog
* Sun Oct 08 2023 Luca Magrone <luca@magrone.cc> - 0-5
- Require bcm2711-firmware instead of the whole bcm283x-firmware

* Thu Dec 08 2022 Luca Magrone <luca@magrone.cc> - 0-4
- Fix missing root device from generated cmdline.txt

* Tue Nov 08 2022 Luca Magrone <luca@magrone.cc> - 0-3
- Fix %%post script not copying initramfs

* Sat Oct 29 2022 Luca Magrone <luca@magrone.cc> - 0-2
- Package should require a kernel and install it in the efi esp root directory

* Sat Oct 29 2022 Luca Magrone <luca@magrone.cc> - 0-1
- Initial package release
